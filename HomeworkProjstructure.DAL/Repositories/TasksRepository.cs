using HomeworkProjstructure.DAL.Interfaces;
using System.Collections.Generic;
using HomeworkProjstructure.DAL.Models;

namespace HomeworkProjstructure.DAL.Repositories
{
    public sealed class TasksRepository : IRepository<Task>
    {
        private List<Task> _entitiesList = new List<Task>();
        private int _entityIdIterator = 1;
        public IEnumerable<Task> GetAll()
        {
            return _entitiesList;
        }

        public Task GetById(int id)
        {
            return _entitiesList.Find(e => e.Id == id);
        }
        public Task Create(Task entity)
        {
            entity.Id = _entityIdIterator++;
            _entitiesList.Add(entity);

            return entity;
        }
        public Task Update(Task entity)
        {
            int entityIndex = _entitiesList.FindIndex(e => e.Id == entity.Id);
            _entitiesList[entityIndex] = entity;
            
            return entity;
        }
        public void Delete(int id)
        {
            int entityIndex = _entitiesList.FindIndex(e => e.Id == id);
            _entitiesList.RemoveAt(entityIndex);
        }
    }
}