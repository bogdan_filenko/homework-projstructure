using HomeworkProjstructure.DAL.Interfaces;
using System.Collections.Generic;
using HomeworkProjstructure.DAL.Models;

namespace HomeworkProjstructure.DAL.Repositories
{
    public sealed class TeamsRepository : IRepository<Team>
    {
        private List<Team> _entitiesList = new List<Team>();
        private int _entityIdIterator = 1;
        public IEnumerable<Team> GetAll()
        {
            return _entitiesList;
        }

        public Team GetById(int id)
        {
            return _entitiesList.Find(e => e.Id == id);
        }
        public Team Create(Team entity)
        {
            entity.Id = _entityIdIterator++;
            _entitiesList.Add(entity);

            return entity;
        }
        public Team Update(Team entity)
        {
            int entityIndex = _entitiesList.FindIndex(e => e.Id == entity.Id);
            
            return entity;
        }
        public void Delete(int id)
        {
            int entityIndex = _entitiesList.FindIndex(e => e.Id == id);
            _entitiesList.RemoveAt(entityIndex);  
        }
    }
}