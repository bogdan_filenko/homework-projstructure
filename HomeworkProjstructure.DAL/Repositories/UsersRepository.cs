using HomeworkProjstructure.DAL.Interfaces;
using System.Collections.Generic;
using HomeworkProjstructure.DAL.Models;

namespace HomeworkProjstructure.DAL.Repositories
{
    public sealed class UsersRepository : IRepository<User>
    {
        private List<User> _entitiesList = new List<User>();
        private int _entityIdIterator = 1;
        public IEnumerable<User> GetAll()
        {
            return _entitiesList;
        }

        public User GetById(int id)
        {
            return _entitiesList.Find(e => e.Id == id);
        }
        public User Create(User entity)
        {
            entity.Id = _entityIdIterator++;
            _entitiesList.Add(entity);

            return entity;
        }
        public User Update(User entity)
        {
            int entityIndex = _entitiesList.FindIndex(e => e.Id == entity.Id);
            _entitiesList[entityIndex] = entity;
            
            return entity;  
        }
        public void Delete(int id)
        {
            int entityIndex = _entitiesList.FindIndex(e => e.Id == id);
            _entitiesList.RemoveAt(entityIndex); 
        }
    }
}