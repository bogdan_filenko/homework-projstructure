using HomeworkProjstructure.DAL.Interfaces;
using System.Collections.Generic;
using HomeworkProjstructure.DAL.Models;

namespace HomeworkProjstructure.DAL.Repositories
{
    public sealed class ProjectsRepository : IRepository<Project>
    {
        private List<Project> _entitiesList = new List<Project>();
        private int _entityIdIterator = 1;
        public IEnumerable<Project> GetAll()
        {
            return _entitiesList;
        }

        public Project GetById(int id)
        {
            return _entitiesList.Find(e => e.Id == id);
        }
        public Project Create(Project entity)
        {
            entity.Id = _entityIdIterator++;
            _entitiesList.Add(entity);

            return entity;
        }
        public Project Update(Project entity)
        {
            int entityIndex = _entitiesList.FindIndex(e => e.Id == entity.Id);
            _entitiesList[entityIndex] = entity;
            
            return entity;
        }
        public void Delete(int id)
        {
            int entityIndex = _entitiesList.FindIndex(e => e.Id == id);
            _entitiesList.RemoveAt(entityIndex);
        }
    }
}