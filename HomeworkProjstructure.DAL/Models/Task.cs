using System;

#nullable enable

namespace HomeworkProjstructure.DAL.Models
{
    public sealed class Task
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public int State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        public Project Project { get; set; }
        public User Performer { get; set; }
    }
}