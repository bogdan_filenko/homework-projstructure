using System;

#nullable enable

namespace HomeworkProjstructure.DAL.Models
{
    public sealed class Project
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public User Author { get; set; }
        public Team Team { get; set; }
    }
}