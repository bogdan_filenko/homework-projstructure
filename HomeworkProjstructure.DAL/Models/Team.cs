using System;

#nullable enable

namespace HomeworkProjstructure.DAL.Models
{
    public sealed class Team
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}