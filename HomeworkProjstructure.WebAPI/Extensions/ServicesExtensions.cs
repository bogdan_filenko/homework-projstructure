using Microsoft.Extensions.DependencyInjection;
using HomeworkProjstructure.BLL.Interfaces;
using HomeworkProjstructure.BLL.Services;
using HomeworkProjstructure.BLL.MappingProfiles;
using HomeworkProjstructure.DAL.Interfaces;
using HomeworkProjstructure.DAL.Repositories;
using HomeworkProjstructure.DAL.Models;
using Microsoft.OpenApi.Models;

namespace HomeworkProjstructure.WebAPI.Extensions
{
    public static class ServicesExtensions
    {
        public static void RegisterRepositories(this IServiceCollection services)
        {
            services.AddSingleton<IRepository<Project>, ProjectsRepository>();
            services.AddSingleton<IRepository<Task>, TasksRepository>();
            services.AddSingleton<IRepository<Team>, TeamsRepository>();
            services.AddSingleton<IRepository<User>, UsersRepository>();
        }

        public static void RegisterBusinessServices(this IServiceCollection services)
        {
            services.AddTransient<IDataComposeService, DataComposeService>();
            services.AddTransient<ILinqService, LinqService>();

            services.AddTransient<IProjectsService, ProjectsService>();
            services.AddTransient<ITasksService, TasksService>();
            services.AddTransient<IUsersService, UsersService>();
            services.AddTransient<ITeamsService, TeamsService>();
        }

        public static void RegisterAutomapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectsProfile>();
                cfg.AddProfile<TeamsProfile>();
                cfg.AddProfile<TasksProfile>();
                cfg.AddProfile<UsersProfile>();
                cfg.AddProfile<LinqProfile>();
            });
        }

        public static void RegisterSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Homework API", Version = "v1"});
            });
        }
    }
}