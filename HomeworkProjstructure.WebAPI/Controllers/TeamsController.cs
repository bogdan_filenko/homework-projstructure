using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using HomeworkProjstructure.BLL.Interfaces;
using HomeworkProjstructure.BLL.DTOs.Team;

namespace HomeworkProjstructure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamsService _teamsService;
        public TeamsController(ITeamsService teamsService)
        {
            _teamsService = teamsService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> Get()
        {
            return Ok(_teamsService.GetTeams());
        }
        
        [HttpGet("{id}")]
        public ActionResult<TeamDTO> Get(int id)
        {
            return Ok(_teamsService.GetTeamById(id));
        }

        [HttpPost]
        public ActionResult<TeamDTO> Post([FromBody] CreateTeamDTO dto)
        {
            return Ok(_teamsService.CreateTeam(dto));
        }

        [HttpPut]
        public ActionResult<TeamDTO> Put([FromBody] UpdateTeamDTO dto)
        {
            return Ok(_teamsService.UpdateTeam(dto));
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _teamsService.DeleteTeam(id);

            return NoContent();
        }
    }
}