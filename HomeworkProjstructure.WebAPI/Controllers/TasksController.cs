using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using HomeworkProjstructure.BLL.Interfaces;
using HomeworkProjstructure.BLL.DTOs.Task;

namespace HomeworkProjstructure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TasksController : ControllerBase
    {
        private readonly ITasksService _tasksService;
        public TasksController(ITasksService tasksService)
        {
            _tasksService = tasksService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> Get()
        {
            return Ok(_tasksService.GetTasks());
        }
        
        [HttpGet("{id}")]
        public ActionResult<TaskDTO> Get(int id)
        {
            return Ok(_tasksService.GetTaskById(id));
        }

        [HttpPost]
        public ActionResult<TaskDTO> Post([FromBody] CreateTaskDTO dto)
        {
            return Ok(_tasksService.CreateTask(dto));
        }

        [HttpPut]
        public ActionResult<TaskDTO> Put([FromBody] UpdateTaskDTO dto)
        {
            return Ok(_tasksService.UpdateTask(dto));
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _tasksService.DeleteTask(id);

            return NoContent();
        }
    }
}