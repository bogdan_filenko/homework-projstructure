using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using HomeworkProjstructure.BLL.Interfaces;
using HomeworkProjstructure.BLL.DTOs.User;

namespace HomeworkProjstructure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _usersService;
        public UsersController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> Get()
        {
            return Ok(_usersService.GetUsers());
        }
        
        [HttpGet("{id}")]
        public ActionResult<UserDTO> Get(int id)
        {
            return Ok(_usersService.GetUserById(id));
        }

        [HttpPost]
        public ActionResult<UserDTO> Post([FromBody] CreateUserDTO dto)
        {
            return Ok(_usersService.CreateUser(dto));
        }

        [HttpPut]
        public ActionResult<UserDTO> Put([FromBody] UpdateUserDTO dto)
        {
            return Ok(_usersService.UpdateUser(dto));
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _usersService.DeleteUser(id);

            return NoContent();
        }
    }
}