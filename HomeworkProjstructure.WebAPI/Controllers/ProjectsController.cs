using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using HomeworkProjstructure.BLL.Interfaces;
using HomeworkProjstructure.BLL.DTOs.Project;

namespace HomeworkProjstructure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectsService _projectsService;
        public ProjectsController(IProjectsService projectsService)
        {
            _projectsService = projectsService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> Get()
        {
            return Ok(_projectsService.GetProjects());
        }
        
        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> Get(int id)
        {
            return Ok(_projectsService.GetProjectById(id));
        }

        [HttpPost]
        public ActionResult<ProjectDTO> Post([FromBody] CreateProjectDTO dto)
        {
            return Ok(_projectsService.CreateProject(dto));
        }

        [HttpPut]
        public ActionResult<ProjectDTO> Put([FromBody] UpdateProjectDTO dto)
        {
            return Ok(_projectsService.UpdateProject(dto));
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _projectsService.DeleteProject(id);

            return NoContent();
        }
    }
}