using System.Collections.Generic;
using HomeworkProjstructure.BLL.Interfaces;
using HomeworkProjstructure.BLL.DTOs.Linq;
using Microsoft.AspNetCore.Mvc;

namespace HomeworkProjstructure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LinqController : ControllerBase
    {
        private readonly ILinqService _linqService;
        
        public LinqController(ILinqService linqService)
        {
            _linqService = linqService;
        }

        [HttpGet("users/{id}/projects/tasks")]
        public ActionResult<IEnumerable<KeyValuePair<ProjectLinqDTO, int>>> GetProjectsTasksNumber(int id)
        {
            return Ok();
        }

        [HttpGet("users/{id}/tasks")]
        public ActionResult<IEnumerable<TaskLinqDTO>> GetAllUserTasks(int id)
        {
            return Ok(_linqService.GetAllUserTasks(id));
        }

        [HttpGet("users/{id}/tasks/finished")]
        public ActionResult<IEnumerable<ShortTaskDTO>> GetUserFinishedTasks(int id)
        {
            return Ok(_linqService.GetFinishedTasks(id));
        }

        [HttpGet("teams/users/ordered")]
        public ActionResult<IEnumerable<ShortTeamDTO>> GetTeamsWhereUsersOlderThan10()
        {
            return Ok(_linqService.GetTeamsWhereUsersOlderThan10());
        }

        [HttpGet("users/tasks/ordered")]
        public ActionResult<IEnumerable<UserLinqDTO>> GetUsersWithTasksOrdered()
        {
            return Ok(_linqService.GetUsersWithSortedTasks());
        }

        [HttpGet("users/{id}/info")]
        public ActionResult<UserInfoDTO> GetUserInfo(int id)
        {
            return Ok(_linqService.GetUserInfo(id));
        }

        [HttpGet("projects/info")]
        public ActionResult<IEnumerable<ProjectInfoDTO>> GetProjectsInfo()
        {
            return Ok(_linqService.GetProjectsInfo());
        }
    }
}