using System;
using System.Text;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using HomeworkProjstructure.Client.Interfaces;

namespace HomeworkProjstructure.Client.Services
{
    public sealed class HttpService : IHttpService
    {
        private readonly HttpClient _client;
        private readonly string _routePrefix;
        
        public HttpService()
        {
            _client = new HttpClient();
            _routePrefix = "https://localhost:5001/api";
        }
        public async Task<T> Get<T>(string uri)
        {
            HttpResponseMessage response = await _client.GetAsync(_routePrefix + uri);
            string message = await response.Content.ReadAsStringAsync();
            
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(message);
            }
            return JsonConvert.DeserializeObject<T>(message);
        }
        public async Task<TEntity> Post<TCreate, TEntity>(string uri, TCreate body)
        {
            HttpResponseMessage response = await _client.PostAsync(_routePrefix + uri, CreateContent<TCreate>(body));
            string message = await response.Content.ReadAsStringAsync();
            
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(message);
            }
            return JsonConvert.DeserializeObject<TEntity>(message);
        }
        public async Task<TEntity> Put<TUpdate, TEntity>(string uri, TUpdate body)
        {
            HttpResponseMessage response = await _client.PutAsync(_routePrefix + uri, CreateContent<TUpdate>(body));
            string message = await response.Content.ReadAsStringAsync();
            
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(message);
            }
            return JsonConvert.DeserializeObject<TEntity>(message);
        }
        public async Task Delete(string uri)
        {
            HttpResponseMessage response = await _client.DeleteAsync(_routePrefix + uri);
            string message = await response.Content.ReadAsStringAsync();
            
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(message);
            }
        }

        private StringContent CreateContent<T>(T body)
        {
            return new StringContent(
                JsonConvert.SerializeObject(body),
                Encoding.UTF8,
                "application/json"
            );
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}