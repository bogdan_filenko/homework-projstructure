using System;

namespace HomeworkProjstructure.Client
{
    public sealed class Cui
    {
        public void ShowMainDialog()
        {
            Console.Clear();
            Console.WriteLine(
                "1. Tasks` number in the user`s project\n" +
                "2. Tasks` list for a determined user with limited name (< 45)\n" +
                "3. Finished tasks` list in the current year for a defined user\n" +
                "4. Teams` list where users older than 10\n" +
                "5. Users` sorted list with sorted tasks\n" +
                "6. User`s info\n" +
                "7. Project info\n" +
                "8. Other operations\n" +
                "9. Exit\n"
            );
        }

        public void ShowOthersDialog()
        {
            Console.Clear();
            Console.WriteLine(
                "1. Projects` menu\n" +
                "2. Tasks` menu\n" +
                "3. Teams` menu\n" +
                "4. Users` menu\n" +
                "5. Back"
            );
        }

        public void ShowProjectsDialog()
        {
            Console.Clear();
            Console.WriteLine(
                "1. Get all projects\n" +
                "2. Get a project by id\n" +
                "3. Create new project\n" +
                "4. Update a project\n" +
                "5. Delete a project\n" +
                "6. Back"
            );
        }
        
        public void ShowTasksDialog()
        {
            Console.Clear();
            Console.WriteLine(
                "1. Get all tasks\n" +
                "2. Get a task by id\n" +
                "3. Create new task\n" +
                "4. Update a task\n" +
                "5. Delete a task\n" +
                "6. Back"
            );
        }

        public void ShowTeamsDialog()
        {
            Console.Clear();
            Console.WriteLine(
                "1. Get all teams\n" +
                "2. Get a team by id\n" +
                "3. Create new team\n" +
                "4. Update a team\n" +
                "5. Delete a team\n" +
                "6. Back"
            );
        }

        public void ShowUsersDialog()
        {
            Console.Clear();
            Console.WriteLine(
                "1. Get all users\n" +
                "2. Get an user by id\n" +
                "3. Create new user\n" +
                "4. Update an user\n" +
                "5. Delete an user\n" +
                "6. Back"
            );
        }

        public void ShowUserIdDialog()
        {
            Console.Clear();
            Console.Write("Please, type user`s id:  ");
        }

        public void ShowProjectIdDialog()
        {
            Console.Clear();
            Console.Write("Please, type project`s id:  ");
        }

        public void ShowTaskIdDialog()
        {
            Console.Clear();
            Console.Write("Please, type task`s id:  ");
        }

        public void ShowTeamIdDialog()
        {
            Console.Clear();
            Console.Write("Please, type team`s id:  ");
        }

        public void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}