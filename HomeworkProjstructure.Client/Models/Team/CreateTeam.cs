using System;

#nullable enable

namespace HomeworkProjstructure.Client.Models.Team
{
    public sealed class CreateTeam
    {
        public string? Name { get; set; }
    }
}