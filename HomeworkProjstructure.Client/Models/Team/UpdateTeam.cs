using System;

#nullable enable

namespace HomeworkProjstructure.Client.Models.Team
{
    public sealed class UpdateTeam
    {
        public int Id { get; set; }
        public string? Name { get; set; }
    }
}