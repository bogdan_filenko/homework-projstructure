using System;

#nullable enable

namespace HomeworkProjstructure.Client.Models.Task
{
    public sealed class CreateTasks
    {
        public string? Name { get; set; }
        public string? Description { get; set; }

        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
    }
}