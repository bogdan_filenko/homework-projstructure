using System;

#nullable enable

namespace HomeworkProjstructure.Client.Models.Task
{
    public sealed class UpdateTask
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public int State { get; set; }

        public int PerformerId { get; set; }
    }
}