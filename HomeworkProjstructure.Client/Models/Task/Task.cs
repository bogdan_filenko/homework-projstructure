using System;

#nullable enable

namespace HomeworkProjstructure.Client.Models.Task
{
    public sealed class Task
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        public int ProjectId { get; set; }
        public int PerformerId { get; set; }

        public override string ToString()
        {
            return "Task:\n" +
                    $"\tId: {this.Id}\n" +
                    $"\tName: {this.Name ?? "None"}\n" +
                    $"\tDescription: {this.Description ?? "None"}\n" +
                    $"\tState: {this.State}\n" +
                    $"\tCreated at: {this.CreatedAt.ToString()}\n" +
                    $"\tFinished at: {this.FinishedAt?.ToString() ?? "Unfinished"}\n" +
                    $"\tProject id: {this.ProjectId}\n" +
                    $"\tPerformer id: {this.PerformerId}";
        }
    }
}