using System;

#nullable enable

namespace HomeworkProjstructure.Client.Models.User
{
    public sealed class UpdateUser
    {
        public int Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public DateTime BirthDate { get; set; }

        public int? TeamId { get; set; }
    }
}