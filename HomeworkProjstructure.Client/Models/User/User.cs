using System;

#nullable enable

namespace HomeworkProjstructure.Client.Models.User
{
    public sealed class User
    {
        public int Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDate { get; set; }

        public int? TeamId { get; set; }

        public override string ToString()
        {
            return "User:\n" +
                    $"\tId: {this.Id}\n" +
                    $"\tFirst name: {this.FirstName ?? "None"}\n" +
                    $"\tLast name: {this.LastName ?? "None"}\n" +
                    $"\tEmail: {this.Email}\n" +
                    $"\tRegistered at: {this.RegisteredAt.ToString()}\n" +
                    $"\tBirth date: {this.BirthDate.ToString()}\n" +
                    $"\tTeam id: {this.TeamId.ToString() ?? "None"}";
        }
    }
}