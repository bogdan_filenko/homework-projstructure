using System;

#nullable enable

namespace HomeworkProjstructure.Client.Models.Linq
{
    public sealed class ProjectLinq
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public TaskLinq[]? Tasks { get; set; }
        public UserLinq Author { get; set; }
        public TeamLinq? Team { get; set; }

        public override string ToString()
        {
            return "Project:\n" +
                    $"\tId: {this.Id}\n" +
                    $"\tName: {this.Name}\n" +
                    $"\tDescription: {this.Description}\n" +
                    $"\tDeadline: {this.Deadline.ToString()}\n" +
                    $"\tCreated at: {this.CreatedAt.ToString()}";
        }
    }
}