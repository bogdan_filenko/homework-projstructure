using System;

#nullable enable

namespace HomeworkProjstructure.Client.Models.Linq
{
    public sealed class TaskLinq
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        public ProjectLinq Project { get; set; }
        public UserLinq Performer { get; set; }

        public override string ToString()
        {
            return  $"Id: {this.Id}\n" +
                    $"Name: {this.Name}\n" +
                    $"Description: {this.Description}\n" +
                    $"Created at: {this.CreatedAt}\n" +
                    $"Finished at: {(this.FinishedAt == default ? "Still in process" : this.FinishedAt.ToString())}\n" +
                    $"Performer: {this.Performer.FirstName + " " + this.Performer.LastName}";
        }
    }
}