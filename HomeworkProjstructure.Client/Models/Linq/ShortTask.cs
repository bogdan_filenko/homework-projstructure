#nullable enable

namespace HomeworkProjstructure.Client.Models.Linq
{
    public sealed class ShortTask
    {
        public int Id { get; set; }
        public string? Name { get; set; }

        public override string ToString()
        {
            return  $"Id: {this.Id}\n" +
                    $"Name: {this.Name}";
        }
    }
}