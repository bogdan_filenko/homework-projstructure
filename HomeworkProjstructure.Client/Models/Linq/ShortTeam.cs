using System.Text;

#nullable enable

namespace HomeworkProjstructure.Client.Models.Linq
{
    public sealed class ShortTeam
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public UserLinq[]? Users { get; set; }

        public override string ToString()
        {
            StringBuilder viewBuilder = new StringBuilder();
            
            viewBuilder.Append( $"Id: {this.Id}\n" +
                                $"Name: {this.Name}\n" +
                                "Users:\n");
            foreach (var user in this.Users)
            {
                viewBuilder.Append(
                    $"\tId: {user.Id}\n" +
                    $"\tName: {user.FirstName + " " + user.LastName}\n" +
                    $"\tEmail: {user.Email}\n\n"
                );
            }
            
            return viewBuilder.ToString();
        }
    }
}