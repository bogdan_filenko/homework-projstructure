using System;
using System.Text;

#nullable enable

namespace HomeworkProjstructure.Client.Models.Linq
{
    public sealed class UserLinq
    {
        public int Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDate { get; set; }

        public TeamLinq? Team { get; set; }
        public ProjectLinq[]? Projects { get; set; }
        public TaskLinq[]? Tasks { get; set; }

        public override string ToString()
        {
            StringBuilder viewBuilder = new StringBuilder();
            
            viewBuilder.Append( $"Id: {this.Id}\n" +
                                $"First name: {this.FirstName}\n" +
                                "Tasks:\n");
            foreach (var task in this.Tasks)
            {
                viewBuilder.Append($"\tId: {task.Id}\n" +
                                    $"\tName: {task.Name}\n");
            }
            
            return viewBuilder.ToString();
        }
    }
}