using System.Text;

namespace HomeworkProjstructure.Client.Models.Linq
{
    public sealed class ProjectInfo
    {
        public ProjectLinq Project { get; set; }
        public TaskLinq LongestByDescTask { get; set; }
        public TaskLinq ShortestByNameTask { get; set; }
        public int TotalProjectParticians { get; set; }

        public override string ToString()
        {
            StringBuilder viewBuilder = new StringBuilder();

            viewBuilder.Append("Project:\n" +
                    $"\tId: {this.Project.Id}\n" +
                    $"\tName: {this.Project.Name}\n");
            if (this.LongestByDescTask != default)
            {
                viewBuilder.Append("The longest project`s task:\n" +
                    $"\tId: {this.LongestByDescTask.Id}\n" +
                    $"\tName: {this.LongestByDescTask.Name}\n");
            }
            else
            {
                viewBuilder.Append("The longest project`s task: None\n");
            }
            
            if (this.ShortestByNameTask != default)
            {
                viewBuilder.Append("The shortest project`s task:\n" +
                    $"\tId: {this.ShortestByNameTask.Id}\n" +
                    $"\tName: {this.ShortestByNameTask.Name}\n");
            }
            else
            {
                viewBuilder.Append("The shortest project`s task: None\n");
            }
            viewBuilder.Append($"Total users` number: {this.TotalProjectParticians}\n");
            
            return viewBuilder.ToString();
        }
    }
}