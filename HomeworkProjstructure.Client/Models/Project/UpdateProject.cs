using System;

#nullable enable

namespace HomeworkProjstructure.Client.Models.Project
{
    public sealed class UpdateProject
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }
    }
}