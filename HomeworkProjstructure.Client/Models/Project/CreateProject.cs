using System;

#nullable enable

namespace HomeworkProjstructure.Client.Models.Project
{
    public sealed class CreateProject
    {
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }

        public int AuthorId { get; set; }
        public int TeamId { get; set; }
    }
}