using System;

#nullable enable

namespace HomeworkProjstructure.Client.Models.Project
{
    public sealed class Project
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public int AuthorId { get; set; }
        public int TeamId { get; set; }

        public override string ToString()
        {
            return "Project:\n" +
                    $"\tId: {this.Id}\n" +
                    $"\tName: {this.Name ?? "None"}\n" +
                    $"\tDescription: {this.Description ?? "None"}\n" +
                    $"\tDeadline: {this.Deadline.ToString()}\n" +
                    $"\tCreated at: {this.CreatedAt.ToString()}\n" +
                    $"\tAuthor id: {this.AuthorId}\n" +
                    $"\tTeam id: {this.TeamId}";
        }
    }
}