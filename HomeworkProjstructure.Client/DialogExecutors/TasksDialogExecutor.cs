using System;
using System.Collections.Generic;
using HomeworkProjstructure.Client.Models;
using HomeworkProjstructure.Client.Models.Task;
using HomeworkProjstructure.Client.DialogExecutors.Abstract;

namespace HomeworkProjstructure.Client.DialogExecutors
{
    public sealed class TasksDialogExecutor : DialogExecutor
    {

        public TasksDialogExecutor(Cui cui) : base(cui)
        {
        }

        public override async System.Threading.Tasks.Task Execute()
        {
            while(_isOpened)
            {
                _cui.ShowTasksDialog();
                char option = Console.ReadKey(true).KeyChar;
                
                try
                {
                    switch(option)
                    {
                        case '1':
                        {
                            var tasks = await _httpService.Get<IEnumerable<Task>>("/Tasks");
                            _dataViewer.ViewCollectionData<Task>(tasks);

                            ResetScreen();
                            break;
                        }
                        case '2':
                        {
                            int? taskId = GetTaskId();
                            if (taskId.HasValue)
                            {
                                var task = await _httpService.Get<Task>($"/Tasks/{taskId.Value}");
                                _dataViewer.ViewSingleEntity<Task>(task);
                            }
                            ResetScreen();
                            break;
                        }
                        case '3':
                        {
                            var task = await _httpService.Post<CreateTasks, Task>("/Tasks", ExecuteCreateTaskDialog());
                            _dataViewer.ViewSingleEntity<Task>(task);

                            ResetScreen();
                            break;
                        }
                        case '4':
                        {
                            int? taskId = GetTaskId();
                            if (taskId.HasValue)
                            {
                                var oldTask = await _httpService.Get<Task>($"/Tasks/{taskId.Value}");

                                var task = await _httpService.Put<UpdateTask, Task>("/Tasks", ExecuteUpdateTaskDialog(oldTask));
                                _dataViewer.ViewSingleEntity<Task>(task);
                            }
                            ResetScreen();
                            break;
                        }
                        case '5':
                        {
                            int? taskId = GetTaskId();
                            if (taskId.HasValue)
                            {
                                await _httpService.Delete($"/Tasks/{taskId.Value}");
                            }
                            ResetScreen();
                            break;
                        }
                        case '6':
                        {
                            _isOpened = false;
                            break;
                        }
                        default:
                        {
                            _cui.ShowMessage("Unknown option");
                            ResetScreen();
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _cui.ShowMessage(ex.Message);
                    ResetScreen();
                }
            }
        }

        private int? GetTaskId()
        {
            _cui.ShowTaskIdDialog();

            if (!int.TryParse(Console.ReadLine(), out int taskId))
            {
                _cui.ShowMessage("Invalid task`s id format");
                return default;
            }
            return taskId;
        }

        private CreateTasks ExecuteCreateTaskDialog()
        {
            _cui.ShowMessage("Type task`s name");
            string name = Console.ReadLine();

            _cui.ShowMessage("Type task`s description");
            string description = Console.ReadLine();

            DateTime deadline = DateTime.Now.AddMonths(6);

            _cui.ShowMessage("Type task`s project id");
            if (!int.TryParse(Console.ReadLine(), out int projectId))
            {
                throw new Exception("Invalid format of project`s id");
            }

            _cui.ShowMessage("Type task`s performer id");
            if (!int.TryParse(Console.ReadLine(), out int performerId))
            {
                throw new Exception("Invalid format of performer`s id");
            }

            return new CreateTasks
            {
                Name = name,
                Description = description,
                ProjectId = projectId,
                PerformerId = performerId
            };
        }

        private UpdateTask ExecuteUpdateTaskDialog(Task oldTask)
        {
            _cui.ShowMessage("Type task`s name");
            string name = Console.ReadLine();

            _cui.ShowMessage("Type task`s description");
            string description = Console.ReadLine();

            _cui.ShowMessage("Type task`s status code");
            if (!int.TryParse(Console.ReadLine(), out int stateCode) && stateCode >= 0 && stateCode <= 3)
            {
                throw new Exception("Invalid format of state`s code");
            }

            _cui.ShowMessage("Type task`s performer id");
            if (!int.TryParse(Console.ReadLine(), out int performerId))
            {
                throw new Exception("Invalid format of performer`s id");
            }
            
            return new UpdateTask
            {
                Id = oldTask.Id,
                Name = string.IsNullOrEmpty(name) ? oldTask.Name : name,
                Description = string.IsNullOrEmpty(description) ? oldTask.Description : description,
                State = stateCode,
                PerformerId = performerId
            };
        }
    }
}