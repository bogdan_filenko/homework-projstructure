using System;
using System.Collections.Generic;
using HomeworkProjstructure.Client.Models.Project;
using HomeworkProjstructure.Client.DialogExecutors.Abstract;

namespace HomeworkProjstructure.Client.DialogExecutors
{
    public sealed class ProjectsDialogExecutor : DialogExecutor
    {

        public ProjectsDialogExecutor(Cui cui) : base(cui)
        {
        }

        public override async System.Threading.Tasks.Task Execute()
        {
            while(_isOpened)
            {
                _cui.ShowProjectsDialog();
                char option = Console.ReadKey(true).KeyChar;

                try
                {
                    switch(option)
                    {
                        case '1':
                        {
                            var projects = await _httpService.Get<IEnumerable<Project>>("/Projects");
                            _dataViewer.ViewCollectionData<Project>(projects);

                            ResetScreen();
                            break;
                        }
                        case '2':
                        {
                            int? projectId = GetProjectId();
                            if (projectId.HasValue)
                            {
                                var project = await _httpService.Get<Project>($"/Projects/{projectId.Value}");
                                _dataViewer.ViewSingleEntity<Project>(project);
                            }
                            ResetScreen();
                            break;
                        }
                        case '3':
                        {
                            var project = await _httpService.Post<CreateProject, Project>("/Projects", ExecuteCreateProjectDialog());
                            _dataViewer.ViewSingleEntity<Project>(project);
                            break;
                        }
                        case '4':
                        {
                            int? projectId = GetProjectId();
                            if (projectId.HasValue)
                            {
                                var oldProject = await _httpService.Get<Project>($"/Projects/{projectId.Value}");

                                var project = await _httpService.Put<UpdateProject, Project>("/Projects", ExecuteUpdateProjectDialog(oldProject));
                                _dataViewer.ViewSingleEntity<Project>(project);
                            }
                            ResetScreen();
                            break;
                        }
                        case '5':
                        {
                            int? projectId = GetProjectId();
                            if (projectId.HasValue)
                            {
                                await _httpService.Delete($"/Projects/{projectId.Value}");
                            }
                            ResetScreen();
                            break;
                        }
                        case '6':
                        {
                            _isOpened = false;
                            break;
                        }
                        default:
                        {
                            _cui.ShowMessage("Unknown option");
                            ResetScreen();
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _cui.ShowMessage(ex.Message);
                    ResetScreen();
                }
            }
        }

        private int? GetProjectId()
        {
            _cui.ShowProjectIdDialog();

            if (!int.TryParse(Console.ReadLine(), out int projectId))
            {
                _cui.ShowMessage("Invalid project`s id format");
                return default;
            }
            return projectId;
        }

        private CreateProject ExecuteCreateProjectDialog()
        {
            _cui.ShowMessage("Type project`s name");
            string name = Console.ReadLine();

            _cui.ShowMessage("Type project`s description");
            string description = Console.ReadLine();

            DateTime deadline = DateTime.Now.AddMonths(6);

            _cui.ShowMessage("Type project`s author id");
            if (!int.TryParse(Console.ReadLine(), out int authorId))
            {
                throw new Exception("Invalid format of author`s id");
            }

            _cui.ShowMessage("Type project`s team id");
            if (!int.TryParse(Console.ReadLine(), out int teamId))
            {
                throw new Exception("Invalid format of team`s id");
            }

            return new CreateProject
            {
                Name = name,
                Description = description,
                Deadline = deadline,
                AuthorId = authorId,
                TeamId = teamId
            };
        }

        private UpdateProject ExecuteUpdateProjectDialog(Project oldProject)
        {
            _cui.ShowMessage("Type project`s name");
            string name = Console.ReadLine();

            _cui.ShowMessage("Type project`s description");
            string description = Console.ReadLine();
            
            return new UpdateProject
            {
                Id = oldProject.Id,
                Name = string.IsNullOrEmpty(name) ? oldProject.Name : name,
                Description = string.IsNullOrEmpty(description) ? oldProject.Description : description,
                Deadline = oldProject.Deadline
            };
        }
    }
}