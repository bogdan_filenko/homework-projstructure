using System;
using System.Collections.Generic;
using HomeworkProjstructure.Client.Models.Linq;
using HomeworkProjstructure.Client.DialogExecutors.Abstract;

namespace HomeworkProjstructure.Client.DialogExecutors
{
    public sealed class MainDialogExecutor : DialogExecutor
    {
        public MainDialogExecutor(Cui cui) : base(cui)
        {
        }

        public override async System.Threading.Tasks.Task Execute()
        {
            while(_isOpened)
            {
                try
                {
                    _cui.ShowMainDialog();
                    char option = Console.ReadKey(true).KeyChar;

                    switch(option)
                    {
                        case '1':
                        {
                            int? userId = ExecuteUserIdDialog();
                            if (userId != default)
                            {
                                _cui.ShowMessage("Not implemented :(");
                            }
                            
                            ResetScreen();
                            break;
                        }
                        case '2':
                        {
                            int? userId = ExecuteUserIdDialog();
                            if (userId != default)
                            {
                                var response = await _httpService.Get<IEnumerable<TaskLinq>>($"/Linq/users/{userId}/tasks");
                                _dataViewer.ViewCollectionData<TaskLinq>(response);
                            }

                            ResetScreen();
                            break;
                        }
                        case '3':
                        {
                            int? userId = ExecuteUserIdDialog();
                            if (userId != default)
                            {
                                var response = await _httpService.Get<IEnumerable<ShortTask>>($"/Linq/users/{userId}/tasks/finished");
                                _dataViewer.ViewCollectionData<ShortTask>(response);
                            }

                            ResetScreen();
                            break;
                        }
                        case '4':
                        {
                            var response = await _httpService.Get<IEnumerable<ShortTeam>>("/Linq/teams/users/ordered");
                            _dataViewer.ViewCollectionData<ShortTeam>(response);
                            
                            ResetScreen();
                            break;
                        }
                        case '5':
                        {
                            var response = await _httpService.Get<IEnumerable<UserLinq>>("/Linq/users/tasks/ordered");
                            _dataViewer.ViewCollectionData<UserLinq>(response);

                            ResetScreen();
                            break;
                        }
                        case '6':
                        {
                            int? userId = ExecuteUserIdDialog();
                            if (userId != default)
                            {
                                var response = await _httpService.Get<UserInfo>($"/Linq/users/{userId}/info");
                                _dataViewer.ViewSingleEntity<UserInfo>(response);
                            }

                            ResetScreen();
                            break;
                        }
                        case '7':
                        {
                            var response = await _httpService.Get<IEnumerable<ProjectInfo>>("/Linq/projects/info");
                            _dataViewer.ViewCollectionData<ProjectInfo>(response);

                            ResetScreen();
                            break;
                        }
                        case '8':
                        {
                            await new OthersDialogExecutor(_cui).Execute();
                            break;
                        }
                        case '9':
                        {
                            _cui.ShowMessage("Program is closing");
                            _isOpened = false;
                            break;
                        }
                        default:
                        {
                            _cui.ShowMessage("Unknown option");
                            ResetScreen();
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _cui.ShowMessage(ex.Message);
                    ResetScreen();
                }
            }
        }

        public int? ExecuteUserIdDialog()
        {
            _cui.ShowUserIdDialog();
            if (!int.TryParse(Console.ReadLine(), out int userId))
            {
                _cui.ShowMessage("Invalid user`s id format");
                return default;
            }
            return userId;
        }
    }
}