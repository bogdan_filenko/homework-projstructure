using System;
using System.Collections.Generic;
using HomeworkProjstructure.Client.Models.User;
using HomeworkProjstructure.Client.DialogExecutors.Abstract;

namespace HomeworkProjstructure.Client.DialogExecutors
{
    public sealed class UsersDialogExecutor : DialogExecutor
    {
        public UsersDialogExecutor(Cui cui) : base(cui)
        {
        }

        public override async System.Threading.Tasks.Task Execute()
        {
            while(_isOpened)
            {
                _cui.ShowUsersDialog();
                char option = Console.ReadKey(true).KeyChar;

                try
                {
                    switch(option)
                    {
                        case '1':
                        {
                            var users = await _httpService.Get<IEnumerable<User>>("/Users");
                            _dataViewer.ViewCollectionData<User>(users);

                            ResetScreen();
                            break;
                        }
                        case '2':
                        {
                            int? userId = GetUserId();
                            if (userId.HasValue)
                            {
                                var user = await _httpService.Get<User>($"/Users/{userId.Value}");
                                _dataViewer.ViewSingleEntity<User>(user);
                            }
                            ResetScreen();
                            break;
                        }
                        case '3':
                        {
                            var user = await _httpService.Post<CreateUser, User>("/Users", ExecuteCreateUserDialog());
                            _dataViewer.ViewSingleEntity<User>(user);

                            ResetScreen();
                            break;
                        }
                        case '4':
                        {
                            int? userId = GetUserId();
                            if (userId.HasValue)
                            {
                                var oldUser = await _httpService.Get<User>($"/Users/{userId.Value}");

                                var user = await _httpService.Put<UpdateUser, User>("/Users", ExecuteUpdateUserDialog(oldUser));
                                _dataViewer.ViewSingleEntity<User>(user);
                            }
                            ResetScreen();
                            break;
                        }
                        case '5':
                        {
                            int? userId = GetUserId();
                            if (userId.HasValue)
                            {
                                await _httpService.Delete($"/Users/{userId.Value}");
                            }
                            ResetScreen();
                            break;
                        }
                        case '6':
                        {
                            _isOpened = false;
                            break;
                        }
                        default:
                        {
                            _cui.ShowMessage("Unknown option");
                            ResetScreen();
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _cui.ShowMessage(ex.Message);
                    ResetScreen();
                }
            }
        }

        private int? GetUserId()
        {
            _cui.ShowUserIdDialog();

            if (!int.TryParse(Console.ReadLine(), out int userId))
            {
                _cui.ShowMessage("Invalid user`s id format");
                return default;
            }
            return userId;
        }

        private CreateUser ExecuteCreateUserDialog()
        {
            _cui.ShowMessage("Type user`s first name");
            string firstName = Console.ReadLine();

            _cui.ShowMessage("Type user`s last name");
            string lastName = Console.ReadLine();

            _cui.ShowMessage("Type user`s email");
            string email = Console.ReadLine();

            _cui.ShowMessage("Type user`s birth date");
            string birthDateStr = Console.ReadLine();
            if (!DateTime.TryParse(birthDateStr, out DateTime birthDate))
            {
                throw new Exception("Invalid format of datetime property");
            }

            int? teamId = null;
            _cui.ShowMessage("Type user`s team id");
            if (int.TryParse(Console.ReadLine(), out int teamIdInt))
            {
                teamId = (int?)teamIdInt;
            }

            return new CreateUser
            {
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                BirthDate = birthDate,
                TeamId = teamId
            };
        }

        private UpdateUser ExecuteUpdateUserDialog(User oldUser)
        {
            _cui.ShowMessage("Type user`s first name");
            string firstName = Console.ReadLine();

            _cui.ShowMessage("Type user`s last name");
            string lastName = Console.ReadLine();

            _cui.ShowMessage("Type user`s email");
            string email = Console.ReadLine();

            _cui.ShowMessage("Type user`s birth date");
            string birthDateStr = Console.ReadLine();
            if (!DateTime.TryParse(birthDateStr, out DateTime birthDate))
            {
                throw new Exception("Invalid format of datetime property");
            }

            int? teamId = oldUser.TeamId;
            _cui.ShowMessage("Type user`s team id");
            if (int.TryParse(Console.ReadLine(), out int teamIdInt))
            {
                teamId = (int?)teamIdInt;
            }
            
            return new UpdateUser
            {
                Id = oldUser.Id,
                FirstName = string.IsNullOrEmpty(firstName) ? oldUser.FirstName : firstName,
                LastName = string.IsNullOrEmpty(lastName) ? oldUser.LastName : lastName,
                Email = string.IsNullOrEmpty(email) ? oldUser.Email : email,
                BirthDate = birthDate,
                TeamId = teamId
            };
        }
    }
}