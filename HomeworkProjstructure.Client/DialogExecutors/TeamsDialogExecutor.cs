using System;
using System.Collections.Generic;
using HomeworkProjstructure.Client.Models.Team;
using HomeworkProjstructure.Client.DialogExecutors.Abstract;

namespace HomeworkProjstructure.Client.DialogExecutors
{
    public sealed class TeamsDialogExecutor : DialogExecutor
    {

        public TeamsDialogExecutor(Cui cui) : base(cui)
        {
        }

        public override async System.Threading.Tasks.Task Execute()
        {
            while(_isOpened)
            {
                _cui.ShowTeamsDialog();
                char option = Console.ReadKey(true).KeyChar;
                try
                {
                    switch(option)
                    {
                        case '1':
                        {
                            var teams = await _httpService.Get<IEnumerable<Team>>("/Teams");
                            _dataViewer.ViewCollectionData<Team>(teams);

                            ResetScreen();
                            break;
                        }
                        case '2':
                        {
                            int? teamId = GetTeamId();
                            if (teamId.HasValue)
                            {
                                var team = await _httpService.Get<Team>($"/Teams/{teamId.Value}");
                                _dataViewer.ViewSingleEntity<Team>(team);
                            }
                            ResetScreen();
                            break;
                        }
                        case '3':
                        {
                            var team = await _httpService.Post<CreateTeam, Team>("/Teams", ExecuteCreateTeamDialog());
                            _dataViewer.ViewSingleEntity<Team>(team);

                            ResetScreen();
                            break;
                        }
                        case '4':
                        {
                            int? teamId = GetTeamId();
                            if (teamId.HasValue)
                            {
                                var oldTeam = await _httpService.Get<Team>($"/Teams/{teamId.Value}");

                                var team = await _httpService.Put<UpdateTeam, Team>("/Teams", ExecuteUpdateTeamDialog(oldTeam));
                                _dataViewer.ViewSingleEntity<Team>(team);
                            }
                            ResetScreen();
                            break;
                        }
                        case '5':
                        {
                            int? teamId = GetTeamId();
                            if (teamId.HasValue)
                            {
                                await _httpService.Delete($"/Teams/{teamId.Value}");
                            }
                            ResetScreen();
                            break;
                        }
                        case '6':
                        {
                            _isOpened = false;
                            break;
                        }
                        default:
                        {
                            _cui.ShowMessage("Unknown option");
                            ResetScreen();
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _cui.ShowMessage(ex.Message);
                    ResetScreen();
                }
            }
        }
        
        private int? GetTeamId()
        {
            _cui.ShowTeamIdDialog();

            if (!int.TryParse(Console.ReadLine(), out int teamId))
            {
                _cui.ShowMessage("Invalid team`s id format");
                return default;
            }
            return teamId;
        }

        private CreateTeam ExecuteCreateTeamDialog()
        {
            _cui.ShowMessage("Type team`s name");
            string name = Console.ReadLine();

            return new CreateTeam
            {
                Name = name
            };
        }

        private UpdateTeam ExecuteUpdateTeamDialog(Team oldTeam)
        {
            _cui.ShowMessage("Type team`s name");
            string name = Console.ReadLine();
            
            return new UpdateTeam
            {
                Id = oldTeam.Id,
                Name = string.IsNullOrEmpty(name) ? oldTeam.Name : name
            };
        }
    }
}