using System;
using System.Threading.Tasks;

namespace HomeworkProjstructure.Client.DialogExecutors
{
    public sealed class OthersDialogExecutor
    {
        private readonly Cui _cui;
        private bool _isOpened;

        public OthersDialogExecutor(Cui cui)
        {
            _isOpened = true;
            _cui = cui;
        }

        public async Task Execute()
        {
            while(_isOpened)
            {
                _cui.ShowOthersDialog();
                char option = Console.ReadKey(true).KeyChar;

                switch(option)
                {
                    case '1':
                    {
                        await new ProjectsDialogExecutor(_cui).Execute();
                        break;
                    }
                    case '2':
                    {
                        await new TasksDialogExecutor(_cui).Execute();
                        break;
                    }
                    case '3':
                    {
                        await new TeamsDialogExecutor(_cui).Execute();
                        break;
                    }
                    case '4':
                    {
                        await new UsersDialogExecutor(_cui).Execute();
                        break;
                    }
                    case '5':
                    {
                        _isOpened = false;
                        break;
                    }
                    default:
                    {
                        _cui.ShowMessage("Unknown option. Press any key to continue...");
                        Console.ReadKey(true);
                        break;
                    }
                }
            }
        }
    }
}