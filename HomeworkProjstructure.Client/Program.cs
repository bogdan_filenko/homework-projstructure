﻿using System.Threading.Tasks;
using HomeworkProjstructure.Client.DialogExecutors;
#nullable enable

namespace HomeworkProjstructure.Client
{
    class Program
    {
        private static readonly Cui _cui = new Cui();

        static async Task Main()
        {
            await new MainDialogExecutor(_cui).Execute();
        }
    }
}
