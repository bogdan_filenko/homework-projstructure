using System;
using System.Threading.Tasks;

namespace HomeworkProjstructure.Client.Interfaces
{
    public interface IHttpService : IDisposable
    {
        Task<T> Get<T>(string uri);
        Task<TEntity> Post<TCreate, TEntity>(string uri, TCreate body);
        Task<TEntity> Put<TUpdate, TEntity>(string uri, TUpdate body);
        Task Delete(string uri);
    }
}