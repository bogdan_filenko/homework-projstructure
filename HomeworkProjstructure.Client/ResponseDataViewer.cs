using HomeworkProjstructure.Client.Models.Linq;
using System.Collections.Generic;
using System;

namespace HomeworkProjstructure.Client
{
    public sealed class ResponseDataViewer
    {
        public void ViewProjectsTaskNumber(IDictionary<ProjectLinq, int> responseData)
        {
            Console.Clear();

            if (responseData != default)
            {
                foreach (var projectNumberPair in responseData)
                {
                    Console.WriteLine(
                        projectNumberPair.Key.ToString() + '\n' +
                        $"Tasks` number: {projectNumberPair.Value}\n"
                        );
                }
            }
        }
        public void ViewCollectionData<T>(IEnumerable<T> responseData)
        {
            Console.Clear();

            if (responseData != default)
            {
                foreach (var entity in responseData)
                {
                    Console.WriteLine(entity.ToString() + '\n');
                }
            }
        }
        public void ViewSingleEntity<T>(T responseData)
        {
            Console.Clear();

            if (responseData != null)
            {
                Console.WriteLine(responseData.ToString() + '\n');
            }
        }
    }
}