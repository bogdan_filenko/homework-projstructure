using System.Collections.Generic;
using AutoMapper;
using HomeworkProjstructure.DAL.Models;
using HomeworkProjstructure.DAL.Interfaces;
using HomeworkProjstructure.BLL.DTOs.Task;
using HomeworkProjstructure.BLL.Interfaces;

namespace HomeworkProjstructure.BLL.Services
{
    public sealed class TasksService : ITasksService
    {
        private readonly IRepository<Task> _tasksRepository;
        private readonly IRepository<Project> _projectsRepository;
        private readonly IRepository<User> _usersRepository;
        private readonly IMapper _mapper;

        public TasksService(
            IRepository<Task> tasksRepository,
            IRepository<Project> projectsRepository,
            IRepository<User> usersRepository,
            IMapper mapper
            )
        {
            _tasksRepository = tasksRepository;
            _projectsRepository = projectsRepository;
            _usersRepository = usersRepository;

            _mapper = mapper;
        }

        public IEnumerable<TaskDTO> GetTasks()
        {
            return _mapper.Map<IEnumerable<TaskDTO>>(_tasksRepository.GetAll());
        }

        public TaskDTO GetTaskById(int id)
        {
            var task = _tasksRepository.GetById(id);
            
            if (task == default)
            {
                return default;
            }
            return _mapper.Map<TaskDTO>(task);
        }

        public TaskDTO CreateTask(CreateTaskDTO createDto)
        {
            Task newTask = _mapper.Map<Task>(createDto);

            newTask.Performer = _usersRepository.GetById(createDto.PerformerId);
            newTask.Project = _projectsRepository.GetById(createDto.ProjectId);;
            
            var createdTask = _tasksRepository.Create(newTask);
            return _mapper.Map<TaskDTO>(createdTask);
        }

        public TaskDTO UpdateTask(UpdateTaskDTO updateDto)
        {
            Task updatableTask = _tasksRepository.GetById(updateDto.Id);
            
            updatableTask.Name = updateDto.Name;
            updatableTask.Description = updateDto.Description;
            updatableTask.State = updateDto.State;
            updatableTask.Performer = _usersRepository.GetById(updateDto.PerformerId);

            var updatedTask = _tasksRepository.Update(updatableTask);
            return _mapper.Map<TaskDTO>(updatedTask);
        }

        public void DeleteTask(int id)
        {
            _tasksRepository.Delete(id);
        }
    }
}