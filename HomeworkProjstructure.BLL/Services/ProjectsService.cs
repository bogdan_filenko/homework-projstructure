using System.Collections.Generic;
using AutoMapper;
using HomeworkProjstructure.DAL.Models;
using HomeworkProjstructure.DAL.Interfaces;
using HomeworkProjstructure.BLL.DTOs.Project;
using HomeworkProjstructure.BLL.Interfaces;

namespace HomeworkProjstructure.BLL.Services
{
    public sealed class ProjectsService : IProjectsService
    {
        private readonly IRepository<Project> _projectsRepository;
        private readonly IRepository<User> _usersRepository;
        private readonly IRepository<Team> _teamsRepository;
        private readonly IMapper _mapper;

        public ProjectsService(
            IRepository<Project> projectsRepository,
            IRepository<User> usersRepository,
            IRepository<Team> teamsRepository,
            IMapper mapper
            )
        {
            _projectsRepository = projectsRepository;
            _usersRepository = usersRepository;
            _teamsRepository = teamsRepository;

            _mapper = mapper;
        }

        public IEnumerable<ProjectDTO> GetProjects()
        {
            return _mapper.Map<IEnumerable<ProjectDTO>>(_projectsRepository.GetAll());
        }

        public ProjectDTO GetProjectById(int id)
        {
            var project = _projectsRepository.GetById(id);
            
            if (project == default)
            {
                return default;
            }
            return _mapper.Map<ProjectDTO>(project);
        }

        public ProjectDTO CreateProject(CreateProjectDTO createDto)
        {
            Project newProject = _mapper.Map<Project>(createDto);

            newProject.Team = _teamsRepository.GetById(createDto.TeamId);
            newProject.Author = _usersRepository.GetById(createDto.AuthorId);
            
            var createdProject = _projectsRepository.Create(newProject);
            return _mapper.Map<ProjectDTO>(createdProject);
        }

        public ProjectDTO UpdateProject(UpdateProjectDTO updateDto)
        {
            Project updatableProject = _projectsRepository.GetById(updateDto.Id);
            
            updatableProject.Name = updateDto.Name;
            updatableProject.Description = updateDto.Description;
            updatableProject.Deadline = updateDto.Deadline;

            var updatedProject = _projectsRepository.Update(updatableProject);
            return _mapper.Map<ProjectDTO>(updatedProject);
        }

        public void DeleteProject(int id)
        {
            _projectsRepository.Delete(id);
        }
    }
}