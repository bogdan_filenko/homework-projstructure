using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using HomeworkProjstructure.BLL.Interfaces;
using HomeworkProjstructure.BLL.DTOs.Linq;
using HomeworkProjstructure.DAL.Models;
using HomeworkProjstructure.DAL.Interfaces;
using HomeworkProjstructure.BLL.Filters;

namespace HomeworkProjstructure.BLL.Services
{
    public class DataComposeService : IDataComposeService
    {
        private readonly IMapper _mapper;

        private IEnumerable<Project> _projectsList;
        private IEnumerable<Task> _tasksList;
        private IEnumerable<Team> _teamsList;
        private IEnumerable<User> _usersList;

        private readonly IRepository<Project> _projectsRepository;
        private readonly IRepository<Task> _tasksRepository;
        private readonly IRepository<Team> _teamsRepository;
        private readonly IRepository<User> _usersRepository;

        private DataComposeFilter _filter;

        public DataComposeService(
            IRepository<Project> projectsRepository,
            IRepository<Task> tasksRepository,
            IRepository<Team> teamsRepository,
            IRepository<User> usersRepository,
            IMapper mapper
            )
        {
            _mapper = mapper;

            _projectsRepository = projectsRepository;
            _tasksRepository = tasksRepository;
            _teamsRepository = teamsRepository;
            _usersRepository = usersRepository;
        }
        public IEnumerable<ProjectLinqDTO> GetComposedDataAsync(DataComposeFilter filter)
        {
            _projectsList = _projectsRepository.GetAll();
            _tasksList = _tasksRepository.GetAll();
            _teamsList = _teamsRepository.GetAll();
            _usersList = _usersRepository.GetAll();

            _filter = filter;

            var composedUsers = GetComposedUsersList();
            var composedTasks = GetComposedTasksList(composedUsers);
            var composedTeams = GetComposedTeamsList(composedUsers);
            var composedProjects = GetComposedProjectsList(composedUsers, composedTeams, composedTasks);

            return composedProjects;
        }

        private IEnumerable<ProjectLinqDTO> GetComposedProjectsList(IEnumerable<UserLinqDTO> composedUsers, IEnumerable<TeamLinqDTO> composedTeams, IEnumerable<TaskLinqDTO> composedTasks)
        {

            var composedProjects = from project in _projectsList
                join team in composedTeams on project.Team.Id equals team.Id
                join user in composedUsers on project.Author.Id equals user.Id
                select _mapper.Map<ProjectLinqDTO>(project, opts =>
                {
                    opts.AfterMap((o, project) =>
                    {
                        project.Team = team;
                        project.Author = user;
                    });
                });
            

            return composedProjects = composedProjects.GroupJoin(
                composedTasks,
                p => p.Id,
                t => t.Project?.Id,
                (p, t) =>
                {
                    p.Tasks = t.ToArray();
                    return p;
                }
            );
        }

        private IEnumerable<TeamLinqDTO> GetComposedTeamsList(IEnumerable<UserLinqDTO> composedUsers)
        {
            var composedTeams = _mapper.Map<IEnumerable<TeamLinqDTO>>(_teamsList);

            if (_filter.IncludeTeamUsers)
            {
                composedTeams = composedTeams.GroupJoin(
                    composedUsers,
                    t => t.Id,
                    u => u.Team?.Id,
                    (t, u) =>
                    {
                        t.Users = u.ToArray();
                        return t;
                    });
            }

            return composedTeams;
        }

        private IEnumerable<TaskLinqDTO> GetComposedTasksList(IEnumerable<UserLinqDTO> composedUsers)
        {
            var composedTasks = _mapper.Map<IEnumerable<TaskLinqDTO>>(_tasksList);

            if (!_filter.IncludeTaskProject)
            {
                composedTasks = composedTasks.Select(u =>
                {
                    u.Project = null;
                    return u;
                });
            }

            return composedTasks;
        }

        private IEnumerable<UserLinqDTO> GetComposedUsersList()
        {
            var composedUsers = _mapper.Map<IEnumerable<UserLinqDTO>>(_usersList);

            if (!_filter.IncludeUserTeam)
            {
                composedUsers = composedUsers.Select(u => 
                {
                    u.Team = null;
                    return u;
                });
            }
            
            if (_filter.IncludeUserTasks)
            {
                composedUsers = composedUsers.GroupJoin(
                    _tasksList,
                    u => u.Id,
                    t => t.Performer.Id,
                    (u, t) =>
                    {
                        u.Tasks = _mapper.Map<TaskLinqDTO[]>(t);
                        return u;
                    }
                );   
            }
            
            if (_filter.IncludeUserProjects)
            {
                composedUsers.GroupJoin(
                    _projectsList,
                    u => u.Id,
                    p => p.Author.Id,
                    (u, p) =>
                    {
                        u.Projects = _mapper.Map<ProjectLinqDTO[]>(p);
                        return u;
                    }
                );
            }

            return composedUsers;
        }
    }
}