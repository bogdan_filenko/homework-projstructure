using System.Collections.Generic;
using AutoMapper;
using HomeworkProjstructure.DAL.Models;
using HomeworkProjstructure.DAL.Interfaces;
using HomeworkProjstructure.BLL.DTOs.Team;
using HomeworkProjstructure.BLL.Interfaces;

namespace HomeworkProjstructure.BLL.Services
{
    public sealed class TeamsService : ITeamsService
    {
        private readonly IRepository<Team> _teamsRepository;
        private readonly IRepository<User> _usersRepository;
        private readonly IMapper _mapper;

        public TeamsService(
            IRepository<Team> teamsRepository,
            IRepository<User> usersRepository,
            IMapper mapper
            )
        {
            _teamsRepository = teamsRepository;
            _usersRepository = usersRepository;

            _mapper = mapper;
        }

        public IEnumerable<TeamDTO> GetTeams()
        {
            return _mapper.Map<IEnumerable<TeamDTO>>(_teamsRepository.GetAll());
        }

        public TeamDTO GetTeamById(int id)
        {
            var team = _teamsRepository.GetById(id);
            
            if (team == default)
            {
                return default;
            }
            return _mapper.Map<TeamDTO>(team);
        }

        public TeamDTO CreateTeam(CreateTeamDTO createDto)
        {
            Team newTeam = _mapper.Map<Team>(createDto);
            
            var createdTeam = _teamsRepository.Create(newTeam);
            return _mapper.Map<TeamDTO>(createdTeam);
        }

        public TeamDTO UpdateTeam(UpdateTeamDTO updateDto)
        {
            Team updatableTeam = _teamsRepository.GetById(updateDto.Id);
           
            updatableTeam.Name = updateDto.Name;

            var updatedTeam = _teamsRepository.Update(updatableTeam);
            return _mapper.Map<TeamDTO>(updatedTeam);
        }

        public void DeleteTeam(int id)
        {
            _teamsRepository.Delete(id);
        }
    }
}