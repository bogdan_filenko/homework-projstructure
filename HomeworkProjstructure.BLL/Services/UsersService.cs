using System.Collections.Generic;
using AutoMapper;
using System.Linq;
using HomeworkProjstructure.DAL.Models;
using HomeworkProjstructure.DAL.Interfaces;
using HomeworkProjstructure.BLL.DTOs.User;
using HomeworkProjstructure.BLL.Interfaces;

namespace HomeworkProjstructure.BLL.Services
{
    public sealed class UsersService : IUsersService
    {
        private readonly IRepository<User> _usersRepository;
        private readonly IRepository<Team> _teamsRepository;
        private readonly IMapper _mapper;

        public UsersService(
            IRepository<User> usersRepository,
            IRepository<Team> teamsRepository,
            IMapper mapper
            )
        {
            _usersRepository = usersRepository;
            _teamsRepository = teamsRepository;

            _mapper = mapper;
        }

        public IEnumerable<UserDTO> GetUsers()
        {
            return _mapper.Map<IEnumerable<UserDTO>>(_usersRepository.GetAll());
        }

        public UserDTO GetUserById(int id)
        {
            var user = _mapper.Map<UserDTO>(_usersRepository.GetById(id));
            
            if (user == default)
            {
                return default;
            }
            return _mapper.Map<UserDTO>(user);
        }

        public UserDTO CreateUser(CreateUserDTO createDto)
        {
            User newUser = _mapper.Map<User>(createDto);

            if (createDto.TeamId.HasValue)
            {   
                newUser.Team = _teamsRepository.GetById(createDto.TeamId.Value);
            }
            
            var createdUser = _usersRepository.Create(newUser);
            return _mapper.Map<UserDTO>(createdUser);
        }

        public UserDTO UpdateUser(UpdateUserDTO updateDto)
        {
            User updatableUser = _usersRepository.GetById(updateDto.Id);

            updatableUser.FirstName = updateDto.FirstName;
            updatableUser.LastName = updateDto.LastName;
            updatableUser.Email = updateDto.Email;
            updatableUser.BirthDate = updateDto.BirthDate;

            if (updateDto.TeamId.HasValue)
            {
                updatableUser.Team = _teamsRepository.GetById(updateDto.TeamId.Value);
            }
            else
            {
                updatableUser.Team = null;
            }

            var updatedUser = _usersRepository.Update(updatableUser);
            return _mapper.Map<UserDTO>(updatedUser);
        }

        public void DeleteUser(int id)
        {
            _usersRepository.Delete(id);
        }
    }
}