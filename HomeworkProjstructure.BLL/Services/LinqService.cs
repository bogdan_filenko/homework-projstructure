using System;
using AutoMapper;
using System.Linq;
using System.Collections.Generic;
using HomeworkProjstructure.BLL.Interfaces;
using HomeworkProjstructure.BLL.DTOs.Linq;
using HomeworkProjstructure.BLL.Filters;

namespace HomeworkProjstructure.BLL.Services
{
    public sealed class LinqService : ILinqService
    {
        private readonly IDataComposeService _dataComposeService;
        private readonly IMapper _mapper;

        private const int CURRENT_YEAR = 2021;
        private const int FINISHED_TASK_CODE = 2;

        public LinqService(IDataComposeService dataComposeService, IMapper mapper)
        {
            _mapper = mapper;
            _dataComposeService = dataComposeService;
        }
        public IDictionary<ProjectLinqDTO, int> GetProjectsTaskNumber(int userId)
        {
            var composedData = _dataComposeService.GetComposedDataAsync(new DataComposeFilter
            {
                IncludeUserTasks = true,
                IncludeUserProjects = true,
                IncludeTeamUsers = true,
                IncludeUserTeam = true,
                IncludeTaskProject = true
            });

            return composedData
                .Where(p => p.Author.Id == userId)
                .ToDictionary(
                    project => project,
                    project => project.Tasks.Length);
        }

        public IEnumerable<TaskLinqDTO> GetAllUserTasks(int userId)
        {
            var composedData = _dataComposeService.GetComposedDataAsync(new DataComposeFilter
            {
                IncludeUserTasks = true,
                IncludeUserProjects = true,
                IncludeTeamUsers = true,
                IncludeUserTeam = true,
                IncludeTaskProject = true
            });

            return composedData
                .SelectMany(p => p.Tasks)
                .Where(t => t.Performer.Id == userId && t.Name.Length < 45);
        }

        public IEnumerable<ShortTaskDTO> GetFinishedTasks(int userId)
        {
            var composedData = _dataComposeService.GetComposedDataAsync(new DataComposeFilter());

            return composedData
                .SelectMany(p => p.Tasks)
                .Where(t => t.FinishedAt != default && t.FinishedAt.Value.Year == CURRENT_YEAR && t.Performer.Id == userId)
                .Select(t => _mapper.Map<ShortTaskDTO>(t));
        }
        
        public IEnumerable<ShortTeamDTO> GetTeamsWhereUsersOlderThan10()
        {
            var composeData = _dataComposeService.GetComposedDataAsync(new DataComposeFilter
            {
                IncludeUserTasks = true,
                IncludeUserProjects = true,
                IncludeTeamUsers = true,
                IncludeUserTeam = true,
                IncludeTaskProject = true
            });

            return composeData
                .Select(t => t.Team)
                .Distinct()
                .Where(t => t.Users.All(u => DateTime.Now.Year - u.BirthDate.Year > 10))
                .Select(t =>
                {
                    var shortTeamDto = _mapper.Map<ShortTeamDTO>(t);
                    shortTeamDto.Users = t.Users.OrderByDescending(u => u.RegisteredAt).ToArray();

                    return shortTeamDto;
                });
        }
        
        public IEnumerable<UserLinqDTO> GetUsersWithSortedTasks()
        {
            var composedData = _dataComposeService.GetComposedDataAsync(new DataComposeFilter
            {
                IncludeUserTasks = true,
                IncludeUserProjects = true,
                IncludeTeamUsers = true,
                IncludeUserTeam = true,
                IncludeTaskProject = true
            });

            return composedData
                .SelectMany(p => p.Team.Users)
                .Select(u =>
                {
                    u.Tasks = u.Tasks.OrderByDescending(t => t.Name.Length).ToArray();
                    return u;
                })
                .OrderBy(u => u.FirstName);
        }

        public UserInfoDTO GetUserInfo(int userId)
        {
            var composedData = _dataComposeService.GetComposedDataAsync(new DataComposeFilter
            {
                IncludeUserTasks = true,
                IncludeUserProjects = true,
                IncludeTeamUsers = true,
                IncludeUserTeam = true,
                IncludeTaskProject = true
            });

            return composedData
                .SelectMany(p => p.Team.Users)
                .Where(u => u.Id == userId)
                .Select(u => new UserInfoDTO()
                {
                    User = u,
                    LastProject = u.Projects
                        ?.OrderByDescending(p => p.CreatedAt)
                        ?.FirstOrDefault() ?? null,
                    LastProjectTotalTasks = u.Projects
                        ?.OrderByDescending(p => p.CreatedAt)
                        ?.FirstOrDefault()?.Tasks?.Length ?? 0,
                    TotalUnfinishedTasks = u?.Tasks.Count(t => t.State != FINISHED_TASK_CODE) ?? 0,
                    LongestTask = u?.Tasks.OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault() ?? null
                })
                .FirstOrDefault();
        }

        public IEnumerable<ProjectInfoDTO> GetProjectsInfo()
        {
            var composedData = _dataComposeService.GetComposedDataAsync(new DataComposeFilter
            {
                IncludeUserTasks = true,
                IncludeUserProjects = true,
                IncludeTeamUsers = true,
                IncludeUserTeam = true,
                IncludeTaskProject = true
            });

            return composedData
                .Where(p => p.Description.Length > 20 || p.Tasks.Length < 3)
                .Select(p => new ProjectInfoDTO()
                {
                    Project = p,
                    LongestByDescTask = p.Tasks
                        .OrderByDescending(t => t.Description)
                        .FirstOrDefault(),
                    ShortestByNameTask = p.Tasks
                        .OrderBy(t => t.Name)
                        .FirstOrDefault(),
                    TotalProjectParticians = p.Team.Users?.Length ?? 0
                });
        }
    }
}