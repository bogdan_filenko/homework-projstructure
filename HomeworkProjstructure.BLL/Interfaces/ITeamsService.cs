using System.Collections.Generic;
using HomeworkProjstructure.BLL.DTOs.Team;

namespace HomeworkProjstructure.BLL.Interfaces
{
    public interface ITeamsService
    {
        IEnumerable<TeamDTO> GetTeams();
        TeamDTO GetTeamById(int id);
        TeamDTO CreateTeam(CreateTeamDTO createDto);
        TeamDTO UpdateTeam(UpdateTeamDTO updateDto);
        void DeleteTeam(int id);
    }
}