using System.Collections.Generic;
using HomeworkProjstructure.BLL.DTOs.Task;

namespace HomeworkProjstructure.BLL.Interfaces
{
    public interface ITasksService
    {
        IEnumerable<TaskDTO> GetTasks();
        TaskDTO GetTaskById(int id);
        TaskDTO CreateTask(CreateTaskDTO createDto);
        TaskDTO UpdateTask(UpdateTaskDTO updateDto);
        void DeleteTask(int id);
    }
}