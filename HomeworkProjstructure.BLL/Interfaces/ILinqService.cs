using System.Collections.Generic;
using System.Threading.Tasks;
using HomeworkProjstructure.BLL.DTOs.Linq;

namespace HomeworkProjstructure.BLL.Interfaces
{
    public interface ILinqService
    {
        IDictionary<ProjectLinqDTO, int> GetProjectsTaskNumber(int userId);

        IEnumerable<TaskLinqDTO> GetAllUserTasks(int userId);

        IEnumerable<ShortTaskDTO> GetFinishedTasks(int userId);
        
        IEnumerable<ShortTeamDTO> GetTeamsWhereUsersOlderThan10();
        
        IEnumerable<UserLinqDTO> GetUsersWithSortedTasks();

        UserInfoDTO GetUserInfo(int userId);

        IEnumerable<ProjectInfoDTO> GetProjectsInfo();
    }
}