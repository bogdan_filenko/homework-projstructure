using HomeworkProjstructure.BLL.Filters;
using System.Collections.Generic;
using HomeworkProjstructure.BLL.DTOs.Linq;

namespace HomeworkProjstructure.BLL.Interfaces
{
    public interface IDataComposeService
    {
        IEnumerable<ProjectLinqDTO> GetComposedDataAsync(DataComposeFilter filter);
    }
}