using System.Collections.Generic;
using HomeworkProjstructure.BLL.DTOs.User;

namespace HomeworkProjstructure.BLL.Interfaces
{
    public interface IUsersService
    {
        IEnumerable<UserDTO> GetUsers();
        UserDTO GetUserById(int id);
        UserDTO CreateUser(CreateUserDTO createDto);
        UserDTO UpdateUser(UpdateUserDTO updateDto);
        void DeleteUser(int id);
    }
}