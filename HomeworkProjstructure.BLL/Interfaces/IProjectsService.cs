using System.Collections.Generic;
using HomeworkProjstructure.BLL.DTOs.Project;

namespace HomeworkProjstructure.BLL.Interfaces
{
    public interface IProjectsService
    {
        IEnumerable<ProjectDTO> GetProjects();
        ProjectDTO GetProjectById(int id);
        ProjectDTO CreateProject(CreateProjectDTO createDto);
        ProjectDTO UpdateProject(UpdateProjectDTO updateDto);
        void DeleteProject(int id);
    }
}