
namespace HomeworkProjstructure.BLL.Filters
{
    public sealed class DataComposeFilter
    {
        public bool IncludeUserTasks { get; set; } = false;
        public bool IncludeUserTeam { get; set; } = false;
        public bool IncludeUserProjects { get; set; } = false;

        public bool IncludeTeamUsers { get; set; } = false;

        public bool IncludeTaskProject { get; set; } = false;
    }
}