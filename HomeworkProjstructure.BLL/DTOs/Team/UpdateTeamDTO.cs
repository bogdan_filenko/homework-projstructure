#nullable enable

namespace HomeworkProjstructure.BLL.DTOs.Team
{
    public sealed class UpdateTeamDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
    }
}