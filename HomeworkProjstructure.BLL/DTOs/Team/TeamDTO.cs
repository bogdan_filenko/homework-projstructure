using System;

#nullable enable

namespace HomeworkProjstructure.BLL.DTOs.Team
{
    public sealed class TeamDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}