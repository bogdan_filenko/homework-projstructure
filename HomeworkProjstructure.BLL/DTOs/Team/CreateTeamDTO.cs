#nullable enable

namespace HomeworkProjstructure.BLL.DTOs.Team
{
    public sealed class CreateTeamDTO
    {
        public string? Name { get; set; }
    }
}