#nullable enable

namespace HomeworkProjstructure.BLL.DTOs.Task
{
    public sealed class CreateTaskDTO
    {
        public string? Name { get; set; }
        public string? Description { get; set; }

        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
    }
}