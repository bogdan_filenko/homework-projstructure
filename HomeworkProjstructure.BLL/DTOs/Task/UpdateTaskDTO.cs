#nullable enable

namespace HomeworkProjstructure.BLL.DTOs.Task
{
    public sealed class UpdateTaskDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public int State { get; set; }

        public int PerformerId { get; set; }
    }
}