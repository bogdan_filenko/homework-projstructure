using System;

#nullable enable

namespace HomeworkProjstructure.BLL.DTOs.Project
{
    public sealed class CreateProjectDTO
    {
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }

        public int AuthorId { get; set; }
        public int TeamId { get; set; }
    }
}