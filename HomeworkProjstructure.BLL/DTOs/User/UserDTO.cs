using System;

#nullable enable

namespace HomeworkProjstructure.BLL.DTOs.User
{
    public sealed class UserDTO
    {
        public int Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDate { get; set; }

        public int? TeamId { get; set; }
    }
}