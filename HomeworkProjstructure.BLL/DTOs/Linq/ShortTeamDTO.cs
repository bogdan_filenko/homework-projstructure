
#nullable enable

namespace HomeworkProjstructure.BLL.DTOs.Linq
{
    public sealed class ShortTeamDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public UserLinqDTO[]? Users { get; set; }
    }
}