#nullable enable

namespace HomeworkProjstructure.BLL.DTOs.Linq
{
    public sealed class ShortTaskDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }

    }
}