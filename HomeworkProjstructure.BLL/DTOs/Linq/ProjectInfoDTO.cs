
namespace HomeworkProjstructure.BLL.DTOs.Linq
{
    public sealed class ProjectInfoDTO
    {
        public ProjectLinqDTO Project { get; set; }
        public TaskLinqDTO LongestByDescTask { get; set; }
        public TaskLinqDTO ShortestByNameTask { get; set; }
        public int TotalProjectParticians { get; set; }
    }
}