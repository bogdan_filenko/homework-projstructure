using System;

#nullable enable

namespace HomeworkProjstructure.BLL.DTOs.Linq
{
    public sealed class ProjectLinqDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public TaskLinqDTO[]? Tasks { get; set; }
        public UserLinqDTO Author { get; set; }
        public TeamLinqDTO? Team { get; set; }
    }
}