using System;

#nullable enable

namespace HomeworkProjstructure.BLL.DTOs.Linq
{
    public sealed class TaskLinqDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public int State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        public ProjectLinqDTO Project { get; set; }
        public UserLinqDTO Performer { get; set; }
    }
}