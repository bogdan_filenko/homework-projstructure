
namespace HomeworkProjstructure.BLL.DTOs.Linq
{
    public sealed class UserInfoDTO
    {
        public UserLinqDTO User { get; set; }
        public ProjectLinqDTO LastProject { get; set; }
        public int LastProjectTotalTasks { get; set; }
        public int TotalUnfinishedTasks { get; set; }
        public TaskLinqDTO LongestTask { get; set; }
    }
}