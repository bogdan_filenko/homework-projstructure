using System;

#nullable enable

namespace HomeworkProjstructure.BLL.DTOs.Linq
{
    public sealed class UserLinqDTO
    {
        public int Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDate { get; set; }

        public TeamLinqDTO? Team { get; set; }
        public ProjectLinqDTO[]? Projects { get; set; }
        public TaskLinqDTO[]? Tasks { get; set; }
    }
}