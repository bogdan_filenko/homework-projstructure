using System;

#nullable enable

namespace HomeworkProjstructure.BLL.DTOs.Linq
{
    public sealed class TeamLinqDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public UserLinqDTO[]? Users { get; set; }
    }
}