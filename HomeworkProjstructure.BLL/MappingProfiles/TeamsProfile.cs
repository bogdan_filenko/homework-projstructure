using System;
using AutoMapper;
using HomeworkProjstructure.BLL.DTOs.Team;
using HomeworkProjstructure.DAL.Models;

namespace HomeworkProjstructure.BLL.MappingProfiles
{
    public sealed class TeamsProfile : Profile
    {
        public TeamsProfile()
        {
            CreateMap<Team, TeamDTO>();

            CreateMap<CreateTeamDTO, Team>()
                .ForMember(dest => dest.CreatedAt, src => src.MapFrom(t => DateTime.Now));
        }
    }
}