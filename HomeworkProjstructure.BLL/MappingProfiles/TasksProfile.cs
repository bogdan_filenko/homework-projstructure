using System;
using AutoMapper;
using HomeworkProjstructure.BLL.DTOs.Task;
using HomeworkProjstructure.DAL.Models;

namespace HomeworkProjstructure.BLL.MappingProfiles
{
    public sealed class TasksProfile : Profile
    {
        private const int FINISHED_TASK_CODE = 3;
        public TasksProfile()
        {
            CreateMap<Task, TaskDTO>()
                .ForMember(dest => dest.PerformerId, src => src.MapFrom(t => t.Performer.Id))
                .ForMember(dest => dest.ProjectId, src => src.MapFrom(t => t.Project.Id));

            CreateMap<CreateTaskDTO, Task>()
                .ForMember(dest => dest.CreatedAt, src => src.MapFrom(t => DateTime.Now))
                .ForMember(dest => dest.State, src => src.MapFrom(t => 0));

            CreateMap<UpdateTaskDTO, Task>()
                .ForMember(dest => dest.FinishedAt, src => src.MapFrom(t => t.State == FINISHED_TASK_CODE ? DateTime.Now : default));
        }
    }
}