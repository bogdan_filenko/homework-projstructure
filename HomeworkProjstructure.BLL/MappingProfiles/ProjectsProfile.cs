using AutoMapper;
using System;
using HomeworkProjstructure.BLL.DTOs.Project;
using HomeworkProjstructure.DAL.Models;

namespace HomeworkProjstructure.BLL.MappingProfiles
{
    public sealed class ProjectsProfile : Profile
    {
        public ProjectsProfile()
        {
            CreateMap<Project, ProjectDTO>()
                .ForMember(dest => dest.AuthorId, src => src.MapFrom(p => p.Author.Id))
                .ForMember(dest => dest.TeamId, src => src.MapFrom(p => p.Team.Id));

            CreateMap<CreateProjectDTO, Project>()
                .ForMember(dest => dest.CreatedAt, src => src.MapFrom(p => DateTime.Now));
        }
    }
}