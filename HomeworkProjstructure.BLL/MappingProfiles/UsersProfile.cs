using System;
using AutoMapper;
using HomeworkProjstructure.BLL.DTOs.User;
using HomeworkProjstructure.DAL.Models;

namespace HomeworkProjstructure.BLL.MappingProfiles
{
    public sealed class UsersProfile : Profile
    {
        public UsersProfile()
        {
            CreateMap<User, UserDTO>()
                .ForMember(dest => dest.TeamId, src => src.MapFrom(u => u.Team == null ? null : (int?)u.Team.Id));

            CreateMap<CreateUserDTO, User>()
                .ForMember(dest => dest.RegisteredAt, src => src.MapFrom(u => DateTime.Now));
        }
    }
}