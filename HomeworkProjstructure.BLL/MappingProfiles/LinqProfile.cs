using AutoMapper;
using HomeworkProjstructure.DAL.Models;
using HomeworkProjstructure.BLL.DTOs.Linq;

namespace HomeworkProjstructure.BLL.MappingProfiles
{
    public sealed class LinqProfile : Profile
    {
        public LinqProfile()
        {
            CreateMap<Project, ProjectLinqDTO>();

            CreateMap<User, UserLinqDTO>();

            CreateMap<Team, TeamLinqDTO>();

            CreateMap<Task, TaskLinqDTO>();

            CreateMap<TaskLinqDTO, ShortTaskDTO>();

            CreateMap<TeamLinqDTO, ShortTeamDTO>();
        }
    }
}